
#include <iostream>

#include "SharedPointer/SharedPointer.hpp"

struct Test {
    Test()
    {
        std::cout << "ctor()" << std::endl;
    }

    ~Test()
    {
        std::cout << "dtor()" << std::endl;
    }
};

int main()
{
    MXR::SharedPointer<Test> ptr{new Test{}};
    MXR::SharedPointer<Test> copy{ptr};
    return 0;
}