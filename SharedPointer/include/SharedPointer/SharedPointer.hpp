#pragma once

#include "SharedPointerImpl/SharedPointerImpl.hpp"

namespace MXR {

template<typename T>
class SharedPointer 
{
public:
    SharedPointer()
        : m_impl{}
    {}

    SharedPointer(T* ptr) 
        : m_impl{ptr}
    {}

    ~SharedPointer() = default;

    SharedPointer(SharedPointer<T> const& copy) 
        : m_impl{copy.m_impl}
    {}

    SharedPointer<T> operator=(SharedPointer<T> const& copy) 
    {
        m_impl = copy.m_impl;
        return *this;
    }

    SharedPointer(SharedPointer<T>&&) = delete;

private:
    Impl::SharedPointerImpl<T> m_impl;
};

}