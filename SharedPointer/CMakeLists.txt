add_library(SharedPointerIface INTERFACE)

target_include_directories(SharedPointerIface 
    INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/include/
)