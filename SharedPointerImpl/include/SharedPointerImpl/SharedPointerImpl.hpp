#pragma once

#include <atomic>

namespace MXR {
namespace Impl {

template<typename T>
class SharedPointerImpl
{
public:
    SharedPointerImpl()
        : m_ptr{nullptr}
        , m_counter{nullptr}
    {}

    SharedPointerImpl(T* ptr)
        : m_ptr{ptr}
        , m_counter{new std::atomic<size_t>{1u}}
    {}

    SharedPointerImpl(SharedPointerImpl<T> const& copy) 
        : m_ptr{copy.m_ptr}
        , m_counter{copy.m_counter}
    {
        if (m_counter) 
        {
            m_counter->fetch_add(1u);
        }
    }

    SharedPointerImpl<T> operator=(SharedPointerImpl<T> const& copy) 
    {
        remove();
        m_ptr = copy.m_ptr;
        m_counter = copy.m_counter;
        if (m_counter)
        {
            m_counter->fetch_add(1u);
        }

        return *this;
    }

    ~SharedPointerImpl()
    {
        remove();
    }

private:
    void remove()
    {
        if (m_ptr) 
        {
            if (m_counter->fetch_sub(1u) == 1u) 
            {
                delete m_ptr;
                delete m_counter;
            }
        }
    }

private:
    T* m_ptr;
    std::atomic<size_t>* m_counter;
};

}
}